package com.DS2020_30242_Pop_TudorAndrei_2.entity;

import java.util.Date;
import java.util.Objects;


public class SensorData {

    private Long sensorDataId;
    private Long patientId;
    private Date startTime;
    private Date endTime;
    private String activityType;


    public SensorData(Long sensorDataId, Long patientId, Date startTime, Date endTime, String activityType) {
        this.sensorDataId = sensorDataId;
        this.patientId = patientId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.activityType = activityType;
    }

    public SensorData() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SensorData that = (SensorData) o;
        return patientId.equals(that.patientId) &&
                startTime.equals(that.startTime) &&
                endTime.equals(that.endTime) &&
                activityType.equals(that.activityType);
    }

    @Override
    public String toString() {
        return "SensorData{" +
                "patientId=" + patientId +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", activityType='" + activityType + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(patientId, startTime, endTime, activityType);
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }


    public Long getSensorDataId() {
        return sensorDataId;
    }

    public void setSensorDataId(Long sensorDataId) {
        this.sensorDataId = sensorDataId;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

}
