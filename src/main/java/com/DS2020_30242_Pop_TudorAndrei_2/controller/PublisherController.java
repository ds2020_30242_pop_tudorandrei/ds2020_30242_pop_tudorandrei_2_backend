package com.DS2020_30242_Pop_TudorAndrei_2.controller;

import com.DS2020_30242_Pop_TudorAndrei_2.publisher.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PublisherController {

        @Autowired
        PublisherService publisherService;

        @GetMapping(value = "/sensors/{orice}")
        public String sendSensorData(@PathVariable int orice){
            publisherService.startPublishing();
            return "Sent";
        }
}
