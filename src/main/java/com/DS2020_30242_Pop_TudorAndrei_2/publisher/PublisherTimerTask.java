package com.DS2020_30242_Pop_TudorAndrei_2.publisher;

import com.DS2020_30242_Pop_TudorAndrei_2.entity.SensorData;
import org.apache.juli.AsyncFileHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

@Service
public class PublisherTimerTask extends TimerTask {

    @Autowired
    SensorPublisher publisher;

    Timer myTimer;
    List<SensorData> sensorDataList;
    static int idx = 0;


    synchronized private void incrementIdx(){
        this.idx++;
    }

    synchronized public int getIdx(){
        return idx;
    }

    synchronized public void checkCondition(){
        if(idx >= sensorDataList.size()){
            myTimer.cancel();
        }
    }

    @Override
    public void run() {
        publisher.publish(sensorDataList.get(idx));
        incrementIdx();
        checkCondition();
    }
}
