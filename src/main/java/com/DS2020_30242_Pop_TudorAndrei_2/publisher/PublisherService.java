package com.DS2020_30242_Pop_TudorAndrei_2.publisher;

import com.DS2020_30242_Pop_TudorAndrei_2.entity.SensorData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class PublisherService {

    @Autowired
    private SensorPublisher sensorPublisher;

    @Autowired
    private PublisherTimerTask timerTask;

    public static final String ACTIVITY_FILE = "activity.txt";
    public static final Long PATIENT_ID = 10L;
    private final Timer timer = new Timer();

    List<SensorData> sensorDataList = new ArrayList<>();

    private void populateList() throws FileNotFoundException {
        File file = new File(ACTIVITY_FILE);
        Scanner scanner = new Scanner(file);

        while (scanner.hasNextLine()){
            String activity = scanner.nextLine();
            SensorData sensorData = parseToSensorData(activity, PATIENT_ID);
            sensorDataList.add(sensorData);
        }
        scanner.close();
    }

    private SensorData parseToSensorData(String activity, Long patientId) {
        SensorData sensorData = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String[] components = activity.split("[ \t]+");
        Date startDate = null, stopDate = null;
        try {
            startDate = dateFormat.parse(components[0] + " " + components[1]);
            stopDate = dateFormat.parse(components[2] + " " + components[3]);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        sensorData = new SensorData(null,patientId, startDate, stopDate, components[4]);

        return sensorData;
    }

    public void startPublishing(){
        try {
            populateList();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        timerTask.sensorDataList = this.sensorDataList;
        timerTask.myTimer = this.timer;
        timer.schedule(timerTask, 0L,1000L);
    }


}
