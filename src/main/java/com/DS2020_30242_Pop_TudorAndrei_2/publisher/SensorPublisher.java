package com.DS2020_30242_Pop_TudorAndrei_2.publisher;

import com.DS2020_30242_Pop_TudorAndrei_2.config.MessagingConfig;
import com.DS2020_30242_Pop_TudorAndrei_2.entity.SensorData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class SensorPublisher {

    @Autowired
    private RabbitTemplate template;

    synchronized void publish(SensorData sensorData){
        template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, sensorData);
        System.out.println(sensorData.toString());
    }

}
