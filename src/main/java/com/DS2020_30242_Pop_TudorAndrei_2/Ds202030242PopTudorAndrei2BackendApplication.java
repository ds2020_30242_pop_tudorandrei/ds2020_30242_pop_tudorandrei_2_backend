package com.DS2020_30242_Pop_TudorAndrei_2;

import com.DS2020_30242_Pop_TudorAndrei_2.controller.PublisherController;
import com.DS2020_30242_Pop_TudorAndrei_2.publisher.PublisherService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@SpringBootApplication
@ComponentScan(basePackageClasses = PublisherController.class)
@ComponentScan(basePackageClasses = PublisherService.class)
public class Ds202030242PopTudorAndrei2BackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ds202030242PopTudorAndrei2BackendApplication.class, args);
	}

}
